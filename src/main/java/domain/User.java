package domain;

import utils.UserType;

public class User {

    private long id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private int afm;
    private UserType type;
//    private List<Vehicle> vehicles;

    public User(long id, String email, String password, String firstName, String lastName, String address, int afm, UserType type) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.afm = afm;
        this.type = type;
//        this.vehicles = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAfm() {
        return afm;
    }

    public void setAfm(int afm) {
        this.afm = afm;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

//    public void addVehicleToList(Vehicle vehicle){
//        this.vehicles.add(vehicle);
//    }
//
//    public List getVehicles() {
//        return vehicles;
//    }
//
//    public void setVehicles(List vehicles) {
//        this.vehicles = vehicles;
//    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
//                ", vehicles=" + vehicles +
                ", afm=" + afm +
                ", type='" + type + '\'' +
                '}';
    }
}
