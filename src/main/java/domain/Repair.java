package domain;

import utils.RepairStatus;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Repair {

    private long repairId;
    private LocalDate repairDate;
    private RepairStatus repairStatus;
    private long vehicleId;
    private double repairCost;
    private List<Part> repairParts;

    public Repair(long repairId, LocalDate repairDate, RepairStatus repairStatus, long vehicleId, double repairCost) {
        this.repairId = repairId;
        this.repairDate = repairDate;
        this.repairStatus = repairStatus;
        this.vehicleId = vehicleId;
        this.repairCost = repairCost;
        this.repairParts = new ArrayList<>();
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public long getRepairId() {
        return repairId;
    }

    public void setRepairId(long repairId) {
        this.repairId = repairId;
    }

    public LocalDate getRepairDate() {
        return repairDate;
    }

    public void setRepairDate(LocalDate repairDate) {
        this.repairDate = repairDate;
    }

    public RepairStatus getRepairStatus() {
        return repairStatus;
    }

    public void setRepairStatus(RepairStatus repairStatus) {
        this.repairStatus = repairStatus;
    }

    public double getRepairCost() {
        return repairCost;
    }

    public void setRepairCost(double repairCost) {
        this.repairCost = repairCost;
    }

    public void addPartsToList(Part part){
        this.repairParts.add(part);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "repairId=" + repairId +
                ", repairDate=" + repairDate +
                ", repairStatus='" + repairStatus + '\'' +
                ", vehicleId=" + vehicleId +
                ", repairCost=" + repairCost +
//                ", repairParts=" + repairParts +
                '}';
    }
}
