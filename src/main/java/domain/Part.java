package domain;

public class Part {

    public Part(long partId, String partType, double partCost, long repairId) {
        this.partId = partId;
        this.partType = partType;
        this.partCost = partCost;
        this.repairId = repairId;
    }

    private long partId;
    private String partType;
    private double partCost;

    public long getRepairId() {
        return repairId;
    }

    public void setRepairId(long repairId) {
        this.repairId = repairId;
    }

    private long repairId;

    public long getPartId() {
        return partId;
    }

    public void setPartId(long partId) {
        this.partId = partId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public double getPartCost() {
        return partCost;
    }

    public void setPartCost(double partCost) {
        this.partCost = partCost;
    }


    @Override
    public String toString() {
        return "Part{" +
                "partId=" + partId +
                ", partType='" + partType + '\'' +
                ", partCost=" + partCost +
                ", repairId=" + repairId +
                '}';
    }
}
