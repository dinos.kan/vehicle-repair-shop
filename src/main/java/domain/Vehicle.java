package domain;

import services.RepairService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Vehicle {

    private long vehicleId;
    private String brand;
    private String model;
    private String plateNumber;
    private LocalDate creationDate;
    private String color;
    private int price;
    private long ownerId;

    public Vehicle(long vehicleId, String brand, String model, LocalDate creationDate, String plateNumber, String color, int price, long ownerId) {
        this.vehicleId = vehicleId;
        this.brand = brand;
        this.model = model;
        this.plateNumber = plateNumber;
        this.creationDate = creationDate;
        this.color = color;
        this.price = price;
        this.ownerId = ownerId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleId=" + vehicleId +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", creationDate=" + creationDate +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", ownerId='" + ownerId + '\'' +
                '}';
    }
}
