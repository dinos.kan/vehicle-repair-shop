package domain;

import services.PartService;
import services.RepairService;
import services.UserService;
import services.VehicleService;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        UserService userService = new UserService();
        VehicleService vehicleService = new VehicleService();
        RepairService repairService = new RepairService();
        PartService partService = new PartService();

        //DATES
        LocalDate date1 = LocalDate.of(2015, 12, 20);
        LocalDate date2 = LocalDate.of(2010, 10, 10);
        LocalDate date3 = LocalDate.of(2005, 8, 15);
        LocalDate date4 = LocalDate.of(2018, 11, 17);
        LocalDate date5 = LocalDate.of(1999, 4, 13);
        LocalDate date6 = LocalDate.of(2007, 6, 15);
        LocalDate date7 = LocalDate.of(2006, 8, 17);
        LocalDate date8 = LocalDate.of(2011, 10, 19);
        LocalDate date9 = LocalDate.of(2014, 1, 21);
        LocalDate date10 = LocalDate.of(2015, 10, 5);


        //VEHICLES
        vehicleService.createVehicle(1, "Alfa Romeo", "156", "ZZZ9008", date1, "white", 5000, 17111993);
        vehicleService.createVehicle(2, "Audi", "A1", "KMT1718", date2, "red", 10500, 15021975);
        vehicleService.createVehicle(3, "Audi", "TT", "ZKN7890", date3, "black", 15000, 30091993);
        vehicleService.createVehicle(4, "BMW", "Z4", "KAM1711", date4, "white", 28900, 17111993);


        //USERS
        userService.createUser(15021975, "takis@takis.com", "takis", "Panagiotis", "Tokos",
                "Egaleo", 123456789,"SimpleUser" );
        userService.createUser(17111993, "dinos@dinos.com", "dinos", "Konstantinos", "Kanakakis",
                "Egaleo", 160010264,"SimpleUser");
        userService.createUser(30091993, "makis@makis.com", "makis", "Prodromos", "Sakis",
                "Peristeri", 987654321, "Administrator");
        userService.createUser(17121987, "nana@nana.com", "nana", "Anastasia", "Kanakaki",
                "Chaidari", 567891234, "SimpleUser");
        User userToUpdate = new User(15021975, "takis@takis.com", "takis", "Panagiotis", "Tsoukalas",
                "Pireas", 123456789,"Administrator" );


        //REPAIRS
        repairService.createRepair(10, date5, "Finished", 1, 60);
        repairService.createRepair(11, date6, "Pending", 2, 30.2);
        repairService.createRepair(12, date6, "Finished", 1, 25.7);
        repairService.createRepair(13, date7, "Pending", 3, 15);


        //PARTS
        partService.createPart(91,"engine", 300, 10);
        partService.createPart(95, "wheel", 90.5, 10);
        partService.createPart(92, "sensors", 45.4, 11);
        partService.createPart(90, "airbag", 65,13);



        //Methods calling
        boolean b = userService.removeUserFromList(17121987);
        System.out.println(b);

        userService.updateUser(userToUpdate, 15021975);

        List<User> allUsers = userService.findAllUsers();
        System.out.println(allUsers);

        List<User> users = userService.searchUserByData("Konstantinos");
        System.out.println(users.toString());

        User user = userService.searchUserById(17111993);
        System.out.println(user);

        Repair repair = repairService.findRepairById(10);
        System.out.println(repair);

        List<Repair> repairs = repairService.findAllRepairs();
        System.out.println(repairs);

        List<Vehicle> vehicle = vehicleService.findVehicleByData("white");
        System.out.println(vehicle.toString());

        List<Repair> vehicleRepairs = repairService.findVehicleRepairs(1);
        System.out.println(vehicleRepairs.toString());

        List<Repair> userRepairs = vehicleService.findUserRepairs(17111993);
        System.out.println(userRepairs.toString());

        List<Part> parts = partService.findRepairParts(10);
        System.out.println(parts.toString());

//        List<Part> parts = repairService.findRepairParts(10);
//        System.out.println(parts.toString());

        List<Vehicle> vehicles = vehicleService.findUserVehicles(30091993);
        System.out.println(vehicles);

        double totalCost = repairService.calcTotalCost(10);
        System.out.println(totalCost);

    }
}
