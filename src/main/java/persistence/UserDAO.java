package persistence;

import domain.User;
import utils.UserType;

import java.util.List;

public interface UserDAO {
    void insertUser(long userId, String email, String password, String firstName, String lastName,
                    String address, int afm, UserType type);

    boolean deleteUserFromList(long id);

    boolean updateUser(User userToUpdate);

    List<User> findAllUsers();

    User findUserById(long userId);

    List<User> findUserByData(String givenData);

    User findUserByCredentials(String email, String password);
}
