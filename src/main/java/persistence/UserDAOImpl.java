package persistence;

import domain.User;
import utils.UserType;

import java.util.*;

public class UserDAOImpl implements UserDAO {

    private static List<User> usersList = new ArrayList();

    private void addUserToList(User user){
        usersList.add(user);
    }

    @Override
    public void insertUser(long userId, String email, String password, String firstName, String lastName,
                           String address, int afm, UserType type) {
        User user = new User(userId, email, password, firstName, lastName, address, afm, type);

        if(findUserById(userId) == null) {
            addUserToList(user);
        }
    }

    @Override
    public boolean deleteUserFromList(long id){
        boolean bool = false;
        Iterator<User> userIterator = usersList.iterator();
        while (userIterator.hasNext()){
            if(userIterator.next().getId() == id){
                userIterator.remove();
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public boolean updateUser(User userToUpdate){
        for (User user:usersList){
            if (user.getId() == userToUpdate.getId()){
                updateFields(userToUpdate, user);
                return true;
            }
        }
        return false;
    }

    private void updateFields(User userToUpdate, User user) {
        user.setAddress(userToUpdate.getAddress());
        user.setAfm(userToUpdate.getAfm());
        user.setEmail(userToUpdate.getEmail());
        user.setFirstName(userToUpdate.getFirstName());
        user.setLastName(userToUpdate.getLastName());
        user.setPassword(userToUpdate.getPassword());
    }

    @Override
    public List<User> findAllUsers(){
        return usersList;
    }

    @Override
    public User findUserById(long userId) {
//        for (User user: usersList) {
//            if (user.getId() == userId) {
//                return user;
//            }
//        }
        return usersList.stream()
                .filter(user -> user.getId() == userId)
                .findAny()
                .orElse(null);
    }



    @Override
    public List<User> findUserByData(String givenData) {
        List<User> users = new ArrayList<>();
        for (User user: usersList){
            if(user.getAddress().equals(givenData)){
                users.add(user);
            } else if(user.getEmail().equals(givenData)){
                users.add(user);
            } else if(user.getFirstName().equals(givenData)){
                users.add(user);
            } else if(user.getLastName().equals(givenData)){
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public User findUserByCredentials(String email, String password) {
        for (User user: usersList) {
            if (email.equals(user.getEmail()) && password.equals(user.getPassword())){
                return user;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "UserDAO{" +
                "usersList=" + usersList +
                '}';
    }
}
