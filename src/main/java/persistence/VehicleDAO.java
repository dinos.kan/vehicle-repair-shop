package persistence;

import domain.Vehicle;

import java.time.LocalDate;
import java.util.List;

public interface VehicleDAO {
    void insertVehicle(long vehicleId, String brand, String model, LocalDate creationDate, String plateNumber, String color,
                       int price, long ownerId);

    boolean deleteVehicleFromList(long id);

    boolean updateVehicle(Vehicle vehicleToUpdate);

    List<Vehicle> findAllVehicles();

    Vehicle findVehicleById(long vehicleId);

    List<Vehicle> findVehicleByData(String givenData);

    List<Vehicle> findVehiclesByUserId(long userId);
}
