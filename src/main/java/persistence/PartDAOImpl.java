package persistence;

import domain.Part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PartDAOImpl implements PartDAO {

    private static List<Part> partsList = new ArrayList();

    private void addPartToList(Part part){
        partsList.add(part);
    }

    @Override
    public void insertPart(long partId, String partType, double partCost, long repairId){
        Part part = new Part(partId, partType, partCost, repairId);
        addPartToList(part);
    }

    @Override
    public boolean deletePartFromList(long partId){
        boolean bool = false;
        Iterator<Part> partIterator = partsList.iterator();
        while (partIterator.hasNext()){
            if (partIterator.next().getPartId() == partId){
                partIterator.remove();
                bool = true;
            }
        }
        return bool;
    }

    @Override
    public boolean updatePart(Part partToUpdate){
        for (Part part:partsList){
            if(part.getPartId() == partToUpdate.getPartId()){
                updateFields(partToUpdate, part);
                return true;
            }
        }
        return false;
    }

    private void updateFields(Part partToUpdate, Part part) {
        part.setPartCost(partToUpdate.getPartCost());
        part.setPartType(partToUpdate.getPartType());
        part.setRepairId(partToUpdate.getRepairId());
    }

    @Override
    public List<Part> findAllParts(){
        return partsList;
    }

    @Override
    public Part findPartById(long partId){
        for (Part part: partsList){
            if(part.getPartId()==partId){
                return part;
            } else
                System.out.println("Given ID does not exist.");
        }
        return null;
    }

    @Override
    public Part findPartByType(String partType){
        for (Part part: partsList){
            if(part.getPartType().equals(partType)){
                return part;
            } else
                System.out.println("Given part type does not exist.");
        }
        return null;
    }

    @Override
    public Part findPartByCost(int partCost){
        for (Part part: partsList){
            if (part.getPartCost() == partCost){
                return part;
            }
        }
        return null;
    }

    @Override
    public List<Part> findRepairParts(long repairId){
        List<Part> repairParts = new ArrayList<>();
        for (Part part: partsList){
            if (repairId == part.getRepairId()){
                repairParts.add(part);
            }
        }
        return repairParts;
    }

    public double calcRepairPartsCost(long repairId){
        double partsCost = 0.0;
        for (Part part: partsList){
            if (repairId == part.getRepairId()){
                partsCost = partsCost + part.getPartCost();
            }
        }
        return partsCost;
    }
}