package persistence;

import domain.Repair;
import utils.RepairStatus;

import java.time.LocalDate;
import java.util.List;

public interface RepairDAO {
    void insertRepair(long repairId, LocalDate repairDate, RepairStatus repairStatus,
                      long vehicleId, double repairCost);

    boolean deleteRepairFromList(long repairId);

    boolean updateRepair(Repair repairToUpdate);

    List<Repair> findAllRepairs();

    Repair findRepairById(long repairId);

    List<Repair> findVehicleRepairs(long vehicleId);

    double calcRepairCost(long repairId);
}
