package utils;

public enum UserType {
    SIMPLE_USER,
    ADMINISTRATOR;
}
