import domain.*;
import services.PartService;
import services.RepairService;
import services.UserService;
import services.VehicleService;

import java.time.LocalDate;
import java.util.List;

import static utils.RepairStatus.*;
import static utils.UserType.ADMINISTRATOR;
import static utils.UserType.SIMPLE_USER;

public class Main {

    public static void main(String[] args) {

        UserService userService = new UserService();
        VehicleService vehicleService = new VehicleService();
        RepairService repairService = new RepairService();
        PartService partService = new PartService();

        //DATES
        LocalDate date1 = LocalDate.of(2015, 12, 20);
        LocalDate date2 = LocalDate.of(2010, 10, 10);
        LocalDate date3 = LocalDate.of(2005, 8, 15);
        LocalDate date4 = LocalDate.of(2018, 11, 17);
        LocalDate date5 = LocalDate.of(1999, 4, 13);
        LocalDate date6 = LocalDate.of(2007, 6, 15);
        LocalDate date7 = LocalDate.of(2006, 8, 17);
        LocalDate date8 = LocalDate.of(2011, 10, 19);
        LocalDate date9 = LocalDate.of(2014, 1, 21);
        LocalDate date10 = LocalDate.of(2015, 10, 5);


        //VEHICLES
        vehicleService.insertVehicle(1, "Alfa Romeo", "156", "ZZZ9008", date1, "white", 5000, 17111993);
        vehicleService.insertVehicle(2, "Audi", "A1", "KMT1718", date2, "red", 10500, 15021975);
        vehicleService.insertVehicle(3, "Audi", "TT", "ZKN7890", date3, "black", 15000, 30091993);
        vehicleService.insertVehicle(4, "BMW", "Z4", "KAM1711", date4, "white", 28900, 17111993);


        //USERS
        userService.insertUser(1001, "takis@takis.com", "takis", "Panagiotis", "Tokos",
                "Egaleo", 123456789,SIMPLE_USER);
        userService.insertUser(1002, "dinos@dinos.com", "dinos", "Konstantinos", "Kanakakis",
                "Egaleo", 160010264, SIMPLE_USER);
        userService.insertUser(1003, "makis@makis.com", "makis", "Prodromos", "Sakis",
                "Peristeri", 987654321, ADMINISTRATOR);
        userService.insertUser(1004, "nana@nana.com", "nana", "Anastasia", "Kanakaki",
                "Chaidari", 567891234, SIMPLE_USER);
        User userToUpdate = new User(1001, "takis@takis.com", "takis", "Panagiotis", "Tsoukalas",
                "Pireas", 123456789, ADMINISTRATOR);


        //REPAIRS
        repairService.insertRepair(10, date5, FINISHED, 1, 60);
        repairService.insertRepair(11, date6, IN_PROGRESS, 2, 30.2);
        repairService.insertRepair(12, date6, SCHEDULED, 1, 25.7);
        repairService.insertRepair(13, date7, IN_PROGRESS, 3, 15);


        //PARTS
        partService.insertPart(91,"engine", 300, 10);
        partService.insertPart(92, "wheel", 90.5, 10);
        partService.insertPart(93, "sensors", 45.4, 11);
        partService.insertPart(94, "airbag", 65,13);


        //Methods calling
//        List<User> allUsers = userService.findAllUsers();
//        System.out.println(allUsers);
//
//        List<Vehicle> allVehicles = vehicleService.findAllVehicles();
//        System.out.println(allVehicles);
//
//        List<Repair> allRepairs = repairService.findAllRepairs();
//        System.out.println(allRepairs);
//
//        List<Part> allParts = partService.findAllParts();
//        System.out.println(allParts);
//
//        boolean b = userService.deleteUserFromList(1004);
//        System.out.println(b);
//
//        boolean bb = userService.updateUser(userToUpdate);
//        System.out.println(bb);
//
//        List<User> allUsersUpdated = userService.findAllUsers();
//        System.out.println(allUsersUpdated);
////
//        List<User> users = userService.findUserByData("Konstantinos");
//        System.out.println(users.toString());
//
        User user = userService.findUserById(1001);
        System.out.println(user);
//
//        Repair repair = repairService.findRepairById(10);
//        System.out.println(repair);
//
//        Vehicle vehicle = vehicleService.findVehicleById(1);
//        System.out.println(vehicle);
//
//        List<Vehicle> vehicle1 = vehicleService.findVehicleByData("white");
//        System.out.println(vehicle1.toString());
//
//        List<Repair> vehicleRepairs = repairService.findVehicleRepairs(1);
//        System.out.println(vehicleRepairs.toString());
//
//        List<Repair> userRepairs = vehicleService.findUserRepairs(17111993);
//        System.out.println(userRepairs.toString());
//
//        List<Part> parts = partService.findRepairParts(10);
//        System.out.println(parts.toString());
//
//        List<Part> parts1 = repairService.findRepairParts(10);
//        System.out.println(parts1.toString());
//
//        List<Vehicle> vehicles = vehicleService.findUserVehicles(30091993);
//        System.out.println(vehicles);
//
//        double totalCost = repairService.calcTotalCost(10);
//        System.out.println(totalCost);

    }
}
