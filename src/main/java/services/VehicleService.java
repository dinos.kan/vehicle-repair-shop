package services;

import domain.Repair;
import domain.Vehicle;
import persistence.VehicleDAO;
import persistence.VehicleDAOImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class VehicleService {

    private VehicleDAO vehicleDAO = new VehicleDAOImpl();
    private RepairService repairService = new RepairService();

    public void insertVehicle(long id, String brand, String model, String plateNumber,
                              LocalDate creationDate, String color, int price, long ownerId){
        vehicleDAO.insertVehicle(id, brand, model, creationDate, plateNumber, color, price, ownerId);
    }

    public boolean removeVehicleFromList(long vehicleId){
        return vehicleDAO.deleteVehicleFromList(vehicleId);
    }

    public List<Vehicle> findAllVehicles(){
        return vehicleDAO.findAllVehicles();
    }

    public Vehicle findVehicleById(long vehicleId){
        if (vehicleDAO.findVehicleById(vehicleId) == null)
            System.out.println("User with given ID does not exist");
        return vehicleDAO.findVehicleById(vehicleId);
    }

    public List<Vehicle> findVehicleByData(String vehicleData){
        return vehicleDAO.findVehicleByData(vehicleData);
    }

    public boolean updateVehicle(Vehicle newVehicle){
        return vehicleDAO.updateVehicle(newVehicle);
    }

    public List<Vehicle> findUserVehicles(long userId){
        return vehicleDAO.findVehiclesByUserId(userId);
    }

    public List<Repair> findUserRepairs(long userId){

        List<Vehicle> userVehicles = findUserVehicles(userId);
        List<Repair> userRepairs = new ArrayList<>();

        for (Vehicle vehicle:userVehicles){
            long vehicleId = vehicle.getVehicleId();
            userRepairs.addAll(repairService.findVehicleRepairs(vehicleId));
        }
        return userRepairs;
    }
}
