package services;

import domain.User;
import persistence.UserDAOImpl;
import utils.UserType;
import persistence.UserDAO;
import java.util.List;

public class UserService {

    private UserDAO userDAO = new UserDAOImpl();
    private VehicleService vehicleService = new VehicleService();

    public void insertUser(long id, String email, String password, String firstName, String lastName,
                           String address, int afm, UserType type) {
        userDAO.insertUser(id, email, password, firstName, lastName, address, afm, type);
    }

    public boolean deleteUserFromList(long id){
        return userDAO.deleteUserFromList(id);
    }

    public List<User> findAllUsers(){
        return userDAO.findAllUsers();
    }

    public User findUserById(long userId){
        if (userDAO.findUserById(userId) == null)
            System.out.println("User with given ID does not exist");
        return userDAO.findUserById(userId);
    }

    public List<User> findUserByData(String givenData) {
        return userDAO.findUserByData(givenData);
    }

    public User findUserByCredentials(String email, String password) {
        return userDAO.findUserByCredentials(email, password);
    }

    public boolean updateUser(User newUser){
        return userDAO.updateUser(newUser);
    }

}
