package services;

import domain.Part;
import persistence.PartDAOImpl;

import java.util.List;

public class PartService {

    private PartDAOImpl partDAOImpl = new PartDAOImpl();
    private static RepairService repairService = new RepairService();

    public void insertPart(long partId, String partType, double partCost, long repairId){
        partDAOImpl.insertPart(partId, partType, partCost, repairId);
    }

    public boolean removePartFromList(long partId){
        return partDAOImpl.deletePartFromList(partId);
    }

    public List<Part> findAllParts(){
       return partDAOImpl.findAllParts();
    }

    public Part findPartById(long partId){
        if (partDAOImpl.findPartById(partId) == null)
            System.out.println("Part with given ID does not exist");
        return partDAOImpl.findPartById(partId);
    }

    public Part findPartByType(String partType){
        return partDAOImpl.findPartByType(partType);
    }

    public Part findPartByCost(int partCost){
        return partDAOImpl.findPartByCost(partCost);
    }

    public boolean updatePart(Part newPart){
        return partDAOImpl.updatePart(newPart);
    }

    public List<Part> findRepairParts(long repairId){
        return partDAOImpl.findRepairParts(repairId);
    }

    public double calcRepairPartsCost(long repairId){
        return partDAOImpl.calcRepairPartsCost(repairId);
    }
}
