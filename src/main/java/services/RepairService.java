package services;

import domain.Part;
import domain.Repair;
import persistence.RepairDAOImpl;
import utils.RepairStatus;
import persistence.RepairDAO;

import java.time.LocalDate;
import java.util.List;

public class RepairService {

    private RepairDAO repairDAO = new RepairDAOImpl();
    private PartService partService = new PartService();

    public void insertRepair(long id, LocalDate repairDate,
                             RepairStatus repairStatus, long vehicleId, double repairCost){
        repairDAO.insertRepair(id, repairDate, repairStatus, vehicleId, repairCost);

    }

    public boolean removeRepairFromList(long id){
        return repairDAO.deleteRepairFromList(id);
    }

    public List<Repair> findAllRepairs(){
        return repairDAO.findAllRepairs();
    }

    public Repair findRepairById(long repairId){
        if (repairDAO.findRepairById(repairId) == null)
            System.out.println("Repair with given ID does not exist");
        return repairDAO.findRepairById(repairId);
    }

    public List<Repair> findVehicleRepairs(long vehicleId){
        return repairDAO.findVehicleRepairs(vehicleId);
    }

    public boolean updateRepair(Repair newRepair){
        return repairDAO.updateRepair(newRepair);
    }

    public List<Part> findRepairParts(long repairId){
        return partService.findRepairParts(repairId);
    }

//    public List<Part> returnPartsList()

    public double calcRepairCost(long repairId){
        return repairDAO.calcRepairCost(repairId);
    }

    public double calcTotalCost(long repairId){
        double repairCost = calcRepairCost(repairId);
        double partsCost = partService.calcRepairPartsCost(repairId);
        return repairCost + partsCost;
    }

}
